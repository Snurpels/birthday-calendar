package de.rpk_aachen.birthday;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.aeonbits.owner.ConfigFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import de.rpk_aachen.birthday.core.BdcConfig;
import de.rpk_aachen.birthday.model.Person;
import de.rpk_aachen.birthday.model.rest.BdcService;
import de.rpk_aachen.birthday.model.tx.AddPerson;
import de.rpk_aachen.birthday.model.tx.Deposit;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class GenerateMockData {
    private final BdcService bdcService;
    private String           password    = "a";
    private Random           rng;
    private String[]         lastNames;
    private String[]         firstNames;
    private List<Person>     people      = new ArrayList<>();
    private List<AddPerson>  txAddPeople = new ArrayList<>();

    private GenerateMockData() {
        BdcConfig cfg = ConfigFactory.create(BdcConfig.class);

        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://" + cfg.hostname() + ":" + cfg.port() + "/server/rest/")
                .addConverterFactory(JacksonConverterFactory.create(mapper)).build();

        bdcService = retrofit.create(BdcService.class);

        firstNames = new String[] { "Caro", "Willie", "Chrissie", "Christie", "Andy", "Marian", "Hubert", "Gottlob",
                "Martin", "Michel", "David", "Heiner", "Ernst", "Florian", "Nikolaus", "Fred", "Arend", };
        lastNames = new String[] { "Stellie", "Wellmie", "Treutli", "Brauer", "Anwalt", "Männerknecht", "Schrei",
                "Jollenbeck", "Fried", "Loris", "Buchholz", "Grosse", "Strohkirch", "Bambach", "Ott", "Schubert",
                "Wedekind", "Houk", "Löwe", };

        rng = new Random(1);
    }

    public static void main(String[] args) throws IOException {
        GenerateMockData mocker = new GenerateMockData();
        mocker.genPeople();
        mocker.genDeposits();
    }

    private void genDeposits() {
        txAddPeople.stream().forEach(ap -> {
            if (rng.nextBoolean()) {
                Deposit d = new Deposit(ap.getPerson(), rng.nextInt(10));
                long age = System.currentTimeMillis() / 1000 - ap.getDate().toEpochSecond(ZoneOffset.ofTotalSeconds(0));
                d.setDate(LocalDateTime.ofEpochSecond(System.currentTimeMillis() / 1000 - rng.nextInt((int) age), 0,
                        ZoneOffset.ofTotalSeconds(0)));
                try {
                    bdcService.addTxDeposit(password, d).execute();
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });
    }

    private void genPeople() throws IOException {
        Set<String> names = new HashSet<String>();
        String name;
        for (int i = 0; i < 40; i++) {
            do {
                name = String.format("%s, %s", lastNames[rng.nextInt(lastNames.length)],
                        firstNames[rng.nextInt(firstNames.length)]);
            } while (names.contains(name));
            names.add(name);
            Person p = new Person(name, LocalDate.of(2012, (rng.nextInt(12) + 1), (rng.nextInt(28) + 1)),
                    rng.nextInt(10) < 2, (rng.nextInt(12) + 1));
            people.add(p);

            bdcService.addPerson(password, p).execute();
            AddPerson ap = new AddPerson(p);
            ap.setDate(LocalDateTime.ofEpochSecond(System.currentTimeMillis() / 1000 - rng.nextInt(30_000_000), 0,
                    ZoneOffset.ofTotalSeconds(0)));
            txAddPeople.add(ap);
            bdcService.addTxAddPerson(password, ap).execute();
        }
    }
}
