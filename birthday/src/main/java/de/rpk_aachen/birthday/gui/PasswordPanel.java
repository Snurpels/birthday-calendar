package de.rpk_aachen.birthday.gui;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;

import net.miginfocom.layout.CC;
import net.miginfocom.swing.MigLayout;

@SuppressWarnings("serial")
public class PasswordPanel extends JPanel {
    private final JPasswordField passwordField = new JPasswordField(12);
    private boolean              gainedFocusBefore;

    public void gainedFocus() {
        if (!gainedFocusBefore) {
            gainedFocusBefore = true;
            passwordField.requestFocusInWindow();
        }
    }

    public PasswordPanel() {
        super(new MigLayout());

        add(new JLabel("<html><div>Einfach auf OK drücken um die Geburtstagsliste nur anzugucken.</div>"),
                new CC().width("280").span(2).wrap());
        add(new JLabel("Password: "));
        add(passwordField);
    }

    public char[] getPassword() {
        return passwordField.getPassword();
    }
}