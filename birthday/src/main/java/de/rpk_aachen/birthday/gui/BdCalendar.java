package de.rpk_aachen.birthday.gui;

import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ItemEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EtchedBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import com.toedter.calendar.JCalendar;

import de.rpk_aachen.birthday.core.ColoredRenderer;
import de.rpk_aachen.birthday.core.DataProvider;
import de.rpk_aachen.birthday.core.ExceptionHandler;
import de.rpk_aachen.birthday.core.Mode;
import de.rpk_aachen.birthday.core.PrintHelper;
import de.rpk_aachen.birthday.core.TextChangeListenerTool;
import de.rpk_aachen.birthday.model.BirthdayTableModel;
import de.rpk_aachen.birthday.model.PeopleComboModel;
import de.rpk_aachen.birthday.model.Person;
import de.rpk_aachen.birthday.model.tx.Payout;
import de.rpk_aachen.birthday.model.tx.Transaction;
import de.rpk_aachen.birthday.model.tx.TransactionTableModel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.miginfocom.layout.CC;
import net.miginfocom.layout.LC;
import net.miginfocom.swing.MigLayout;

@SuppressWarnings("serial")
@RequiredArgsConstructor(onConstructor = @_(@Inject))
public class BdCalendar {
	private interface IKassenPanel {
		public void updateKasse(String kasse);
	}

	private class KassenPanel extends JPanel implements IKassenPanel {
		private JLabel kasse = new JLabel("-");

		@Override
		public void updateKasse(String kasse) {
			this.kasse.setText(kasse);
		}
	}

	private final AddCash ac;
	private final MiscExpense me;
	private BirthdayTableModel bdTableModel;
	private final NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(Locale.getDefault());

	private final DataProvider dp;

	JFrame frame;
	private KassenPanel panel1, panel2, panel3, panel4;
	private final PayBirthday pb;
	private JTabbedPane tabbedPane;
	private TransactionTableModel txTableModel;
	private PeopleComboModel cmbModel;

	private void addUser(JTextField fName, JTextField lName, JCalendar birthday, JSpinner cash) {
		Person p = new Person(lName.getText() + ", " + fName.getText(),
				birthday.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate(), false,
				(Integer) cash.getValue());
		if (JOptionPane.showConfirmDialog(frame,
				String.format("Neue Person %s mit %d € und Geburtstag am %3$td/%3$tm/19XX hinzufügen?", p.getName(),
						p.getEuros(), p.getBirthday())) == JOptionPane.OK_OPTION) {
			dp.addPerson(p);
			fName.setText("");
			lName.setText("");
			cash.setValue(0);
			birthday.setDate(java.sql.Date.valueOf(LocalDate.now()));
			tabbedPane.setSelectedIndex(0);
		}
	}

	private void changeUser(Person pOld, JTextField fName, JTextField lName, JCalendar birthday, JCheckBox alumn,
			JSpinner cash) {
		if (pOld == null)
			return;
		Person pNew = new Person(lName.getText() + ", " + fName.getText(),
				birthday.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate(), alumn.isSelected(),
				(Integer) cash.getValue());
		if (JOptionPane.showConfirmDialog(frame, String.format(
				"Person %s mit %d € und Geburtstag am %3$td/%3$tm/19XX%4$s zu %5$s mit %6$d € und Geburtstag am %7$td/%7$tm/19XX%8$s ändern?",
				pOld.getName(), pOld.getEuros(), pOld.getBirthday(), pOld.isAlumn() ? ", ex" : "", pNew.getName(),
				pNew.getEuros(), pNew.getBirthday(), pNew.isAlumn() ? ", ex" : "")) == JOptionPane.OK_OPTION) {
			dp.changePerson(pOld, pNew);
			fName.setText("");
			lName.setText("");
			cash.setValue(0);
			birthday.setDate(java.sql.Date.valueOf(LocalDate.now()));
			tabbedPane.setSelectedIndex(0);
		}
	}

	/**
	 * Configure the tables sorting based on the current view
	 */
	private void configureSorting(TableModel tableModel, JTable table, int column, SortOrder order) {
		TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(tableModel);
		List<RowSorter.SortKey> sortKeys = new ArrayList<>(25);
		sortKeys.add(new RowSorter.SortKey(column, order));
		if (dp.isAuthed()) {
			sorter.setComparator(1, (o1, o2) -> getEuros(o1) - getEuros(o2));
			sorter.setSortKeys(sortKeys);
			table.setRowSorter(sorter);
			for (int i = 0; i < table.getColumnCount(); i++) {
				sorter.setSortable(i, false);
			}
		}
	}

	public void createAndShowGui() {
		frame = new JFrame("Geburtstagskalender       ");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setIconImage(new ImageIcon(getClass().getResource("/bdc.png")).getImage());

		JPanel content = (JPanel) frame.getContentPane();
		content.setLayout(new GridLayout(1, 1));

		makeDataModel();
		tabbedPane = new JTabbedPane();
		tabbedPane.setToolTipText(null);

		panel1 = makeBirthdayPanel(0, SortOrder.ASCENDING);
		tabbedPane.addTab("Alphabetisch", panel1);

		panel2 = makeBirthdayPanel(1, SortOrder.ASCENDING);
		if (dp.isAuthed()) {
			tabbedPane.addTab("Guthaben", panel2);
		}

		panel3 = makeBirthdayPanel(2, SortOrder.ASCENDING);
		tabbedPane.addTab("Chronologisch", panel3);

		panel4 = makeTransactionPanel();
		tabbedPane.addTab("Transaktionen", panel4);

		JComponent panel5 = makeAddPersonPanel();
		if (dp.isAuthed()) {
			tabbedPane.addTab("Neue Person", panel5);
		}

		JComponent panel6 = makeChangePersonPanel();
		if (dp.isAuthed()) {
			tabbedPane.addTab("Person ändern", panel6);
		}

		content.add(tabbedPane);
		tabbedPane.setTabPlacement(JTabbedPane.RIGHT);

		updateKasse();

		frame.setResizable(false);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

		ExceptionHandler.registerExceptionHandler();
	}

	private int getEuros(Object o) {
		try {
			return currencyFormatter.parse((String) o).intValue();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return 0;
	}

	@SuppressWarnings("deprecation")
	private JComponent makeAddPersonPanel() {
		JPanel panel = new JPanel(false);
		panel.setLayout(new MigLayout(new LC().wrapAfter(2)));

		JLabel disclaimer = new JLabel(
				"<html><br><p align=\"center\">Geburtstage werden aus Datenschutzgründen nur mit Tag/Monat<br>erfasst. Es ist daher nicht nötig, ein Jahr mit anzugeben.</p>");
		disclaimer.setFont(disclaimer.getFont().deriveFont(Font.ITALIC, 11f));
		disclaimer.setHorizontalAlignment(JLabel.CENTER);
		panel.add(disclaimer, new CC().span(2).alignX("center"));

		JLabel label = new JLabel("Anfangsguthaben: ");
		panel.add(label, new CC().growX().height("50"));

		JSpinner cash = new JSpinner();
		panel.add(cash, new CC().minWidth("50").alignX("right"));

		label = new JLabel("Vorname: ");
		panel.add(label, new CC().growX());

		JTextField fName = new JTextField();
		panel.add(fName, new CC().minWidth("150").alignX("right"));

		label = new JLabel("Nachname: ");
		panel.add(label, new CC().growX().pad("-7 0 -7 0"));

		JTextField lName = new JTextField();
		panel.add(lName, new CC().minWidth("150").alignX("right").pad("-7 0 -7 0"));

		JLabel bdLabel = new JLabel(".");
		bdLabel.setFont(bdLabel.getFont().deriveFont(Font.BOLD, 18f));
		bdLabel.setHorizontalAlignment(JLabel.CENTER);
		panel.add(bdLabel, new CC().span(2).growX().alignX("center"));

		label = new JLabel("Geburtstag: ");
		panel.add(label, new CC().alignY("top").growX());

		JCalendar birthday = new JCalendar(new Date(4, 0, 1));
		birthday.getDayChooser()
				.addPropertyChangeListener((PropertyChangeEvent e) -> update(bdLabel, birthday.getDate()));
		birthday.getMonthChooser()
				.addPropertyChangeListener((PropertyChangeEvent e) -> update(bdLabel, birthday.getDate()));
		birthday.getYearChooser()
				.addPropertyChangeListener((PropertyChangeEvent e) -> update(bdLabel, birthday.getDate()));
		birthday.setBackground(panel.getBackground());
		panel.add(birthday, new CC().grow().push());

		panel.add(new JLabel(), new CC().grow().push().wrap());

		JButton ok = new JButton("Hinzufügen");
		ok.addActionListener(ae -> addUser(fName, lName, birthday, cash));
		panel.add(ok, new CC().span(2).grow());

		return panel;
	}

	private KassenPanel makeBirthdayPanel(int column, SortOrder order) {
		KassenPanel panel = new KassenPanel();
		panel.setLayout(new MigLayout());

		JLabel filler = new JLabel("Geburtstage");
		filler.setFont(filler.getFont().deriveFont(Font.BOLD, 28f));
		filler.setHorizontalAlignment(JLabel.CENTER);
		panel.add(filler, new CC().grow().wrap());

		panel.kasse.setFont(panel.kasse.getFont().deriveFont(Font.ITALIC, 11f));
		panel.kasse.setHorizontalAlignment(JLabel.CENTER);
		panel.add(panel.kasse, new CC().grow().wrap());

		JTable table = new JTable(bdTableModel);
		table.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
		JScrollPane scrollPane = new JScrollPane(table);
		table.setFillsViewportHeight(true);
		table.getColumnModel().getColumn(0).setPreferredWidth(200);

		ColoredRenderer rightRenderer = new ColoredRenderer(column == 2 ? Mode.CHRONO : Mode.CREDIT, dp.isAuthed());
		rightRenderer.setHorizontalAlignment(JLabel.RIGHT);
		table.getColumnModel().getColumn(1).setCellRenderer(rightRenderer);
		table.getColumnModel().getColumn(2).setCellRenderer(rightRenderer);

		table.setDefaultRenderer(Object.class,
				new ColoredRenderer(column == 2 ? Mode.CHRONO : Mode.CREDIT, dp.isAuthed()));
		table.setRowSelectionAllowed(false);
		table.setCellSelectionEnabled(false);
		table.setFocusable(false);

		configureSorting(bdTableModel, table, column, order);

		final TableCellRenderer defaultRenderer = table.getTableHeader().getDefaultRenderer();
		table.getTableHeader().setDefaultRenderer((table1, value, isSelected, hasFocus, r, c) -> {
			JLabel label = (JLabel) defaultRenderer.getTableCellRendererComponent(table1, value, isSelected, hasFocus,
					r, c);
			label.setIcon(null);
			return label;
		});

		Icon emptyIcon = new Icon() {
			@Override
			public int getIconHeight() {
				return 0;
			}

			@Override
			public int getIconWidth() {
				return 0;
			}

			@Override
			public void paintIcon(Component c, Graphics g, int x, int y) {
			}
		};
		UIManager.put("Table.ascendingSortIcon", emptyIcon);
		UIManager.put("Table.descendingSortIcon", emptyIcon);

		panel.add(scrollPane, new CC().grow().wrap());

		if (dp.isAuthed()) {
			JButton addCash = new JButton("Geld Einzahlen");
			addCash.addActionListener(e -> ac.createAndShowGui(this, null));
			panel.add(addCash, new CC().growX().wrap());

			JButton payBirthday = new JButton("Geburtstag Auszahlen");
			payBirthday.addActionListener(e -> pb.createAndShowGui(this, null));
			panel.add(payBirthday, new CC().growX().wrap());

			JButton expense = new JButton("Sonstige Ausgaben");
			expense.addActionListener(e -> me.createAndShowGui(this));
			panel.add(expense, new CC().growX().wrap());

			// JButton print = new JButton("Kalender Drucken");
			// print.addActionListener(e -> PrintHelper.print(dp.getPeople()));
			// panel.add(print, new CC().growX().wrap());
		}

		if (dp.isAuthed()) {
			table.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent evt) {
					int rawRow = table.rowAtPoint(evt.getPoint());
					if (rawRow != -1) {
						int row = table.convertRowIndexToModel(rawRow);
						if (rawRow >= 0) {
							ac.createAndShowGui(BdCalendar.this,
									((BirthdayTableModel) table.getModel()).getPerson(row));
						}
					}
				}
			});
		}

		return panel;
	}

	@FunctionalInterface
	private interface Enabler {
		public void setEnabled(boolean isEnabled);
	}

	/**
	 * Wrapper class to customize JComboBox text
	 *
	 */
	@AllArgsConstructor
	@EqualsAndHashCode(of = "person")
	public static class PeopleWrapper {
		@Getter
		Person person;

		@Override
		public String toString() {
			return String.format("%s (%,d €)", person.getName(), person.getEuros());
		}
	}

	@SuppressWarnings("deprecation")
	private JComponent makeChangePersonPanel() {

		JPanel panel = new JPanel(false);
		panel.setLayout(new MigLayout(new LC().wrapAfter(3)));

		JLabel disclaimer = new JLabel(
				"<html><br><p align=\"center\">Geburtstage werden aus Datenschutzgründen nur mit Tag/Monat<br>erfasst. Es ist daher nicht nötig, ein Jahr mit anzugeben.</p>");
		disclaimer.setFont(disclaimer.getFont().deriveFont(Font.ITALIC, 11f));
		disclaimer.setHorizontalAlignment(JLabel.CENTER);
		panel.add(disclaimer, new CC().spanX(3).alignX("center"));

		JComboBox<PeopleWrapper> people = new JComboBox<PeopleWrapper>(cmbModel);
		people.setSelectedIndex(-1);
		panel.add(people, new CC().spanX(3).growX());

		panel.add(new JLabel("Guthaben:"), new CC().alignX("left"));
		JSpinner cash = new JSpinner();
		panel.add(cash, new CC().spanX(2).minWidth("50").alignX("right"));

		panel.add(new JLabel("Ehemalige(r):"), new CC().alignX("left"));
		JCheckBox alumn = new JCheckBox();
		alumn.setHorizontalAlignment(SwingConstants.RIGHT);
		panel.add(alumn, new CC().spanX(2).minWidth("50").alignX("right"));

		JLabel label = new JLabel("Vorname: ");
		panel.add(label, new CC().spanX(2).growX());

		JTextField fName = new JTextField();
		panel.add(fName, new CC().minWidth("150").alignX("right"));

		label = new JLabel("Nachname: ");
		panel.add(label, new CC().spanX(2).growX().pad("-7 0 -7 0"));

		JTextField lName = new JTextField();
		panel.add(lName, new CC().minWidth("150").alignX("right").pad("-7 0 -7 0"));

		JLabel bdLabel = new JLabel(".");
		bdLabel.setFont(bdLabel.getFont().deriveFont(Font.BOLD, 18f));
		bdLabel.setHorizontalAlignment(JLabel.CENTER);
		panel.add(bdLabel, new CC().spanX(3).growX().alignX("center"));

		label = new JLabel("Geburtstag: ");
		panel.add(label, new CC().alignY("top").growX());

		JCalendar birthday = new JCalendar(new Date(4, 0, 1));
		birthday.setBackground(panel.getBackground());
		panel.add(birthday, new CC().spanX(2).grow().push());

		panel.add(new JLabel(), new CC().spanX(3).grow().push());

		panel.add(new JLabel(), new CC().growX().pushX());
		JButton ok = new JButton("Änderungen speichern");
		ok.setEnabled(false);
		ok.addActionListener(ae -> changeUser(((PeopleWrapper) people.getSelectedItem()).person, fName, lName, birthday,
				alumn, cash));
		panel.add(ok, new CC().alignX("right"));
		JButton cancel = new JButton("Änderungen verwerfen");
		cancel.setEnabled(false);
		panel.add(cancel, new CC().alignX("right"));

		Enabler enabler = (e) -> {
			fName.setEnabled(e);
			lName.setEnabled(e);
			cash.setEnabled(e);
			alumn.setEnabled(e);
			birthday.setEnabled(e);
		};
		Enabler buttons = (e) -> {
			ok.setEnabled(e);
			cancel.setEnabled(e);
		};

		enabler.setEnabled(false);

		cancel.addActionListener(ae -> {
			tabbedPane.setSelectedIndex(0);
			people.setSelectedIndex(-1);
			cash.setValue(0);
			alumn.setSelected(false);
			enabler.setEnabled(false);
		});

		birthday.getDayChooser().addPropertyChangeListener((PropertyChangeEvent e) -> {
			update(bdLabel, birthday.getDate());
			if (people.getSelectedItem() != null) {
				Date d1 = Date.from(((PeopleWrapper) people.getSelectedItem()).person.getBirthday()
						.atStartOfDay(ZoneId.systemDefault()).toInstant());
				Date d2 = birthday.getDate();

				buttons.setEnabled(d1.getMonth() != d2.getMonth() || d1.getDate() != d2.getDate());
			} else {
				buttons.setEnabled(false);
			}
		});
		birthday.getMonthChooser().addPropertyChangeListener((PropertyChangeEvent e) -> {
			update(bdLabel, birthday.getDate());
			if (people.getSelectedItem() != null) {
				Date d1 = Date.from(((PeopleWrapper) people.getSelectedItem()).person.getBirthday()
						.atStartOfDay(ZoneId.systemDefault()).toInstant());
				Date d2 = birthday.getDate();

				buttons.setEnabled(d1.getMonth() != d2.getMonth() || d1.getDate() != d2.getDate());
			} else {
				buttons.setEnabled(false);
			}
		});

		people.addItemListener(il -> {
			if (il.getStateChange() == ItemEvent.SELECTED && il.getItem() != null) {
				Person p = ((PeopleWrapper) il.getItem()).person;
				lName.setText(p.getName().split("\\,\\ ")[0]);
				fName.setText(p.getName().split("\\,\\ ")[1]);
				cash.setValue(p.getEuros());
				alumn.setSelected(p.isAlumn());
				birthday.setDate(Date.from(p.getBirthday().atStartOfDay(ZoneId.systemDefault()).toInstant()));
				enabler.setEnabled(true);
			}
		});

		cash.addChangeListener(cl -> {
			boolean changed = people.getSelectedItem() != null && !Integer
					.valueOf(((PeopleWrapper) people.getSelectedItem()).person.getEuros()).equals(cash.getValue());
			buttons.setEnabled(changed);
		});
		//
		// TextChangeListenerTool.addChangeListener(cash, cl -> {
		// boolean changed = people.getSelectedItem() != null && !Integer
		// .valueOf(((PeopleWrapper)
		// people.getSelectedItem()).person.getEuros()).equals(cash.getValue());
		// buttons.setEnabled(changed);
		// });

		TextChangeListenerTool.addChangeListener(lName, cl -> {
			boolean changed = people.getSelectedItem() != null
					&& !((PeopleWrapper) people.getSelectedItem()).person.getName().split("\\,\\ ")[0]
							.equals(lName.getText());
			buttons.setEnabled(changed);
		});

		TextChangeListenerTool.addChangeListener(fName, cl -> {
			boolean changed = people.getSelectedItem() != null
					&& !((PeopleWrapper) people.getSelectedItem()).person.getName().split("\\,\\ ")[1]
							.equals(lName.getText());
			buttons.setEnabled(changed);
		});

		alumn.addActionListener(al -> {
			boolean changed = people.getSelectedItem() != null
					&& alumn.isSelected() != ((PeopleWrapper) people.getSelectedItem()).person.isAlumn();
			buttons.setEnabled(changed);
		});

		return panel;
	}

	private void makeDataModel() {
		bdTableModel = new BirthdayTableModel(dp.isAuthed());
		txTableModel = new TransactionTableModel();
		cmbModel = new PeopleComboModel();
		dp.registerModel(bdTableModel);
		dp.registerModel(txTableModel);
		dp.registerModel(cmbModel);
		bdTableModel.addAll(dp.getPeople().stream().filter(p -> !p.isAlumn()).collect(Collectors.toList()));
		txTableModel.addAll(dp.getTransactions());
	}

	private KassenPanel makeTransactionPanel() {
		KassenPanel panel = new KassenPanel();
		panel.setLayout(new MigLayout());

		JLabel filler = new JLabel("Transaktionen");
		filler.setFont(filler.getFont().deriveFont(Font.BOLD, 28f));
		filler.setHorizontalAlignment(JLabel.CENTER);
		panel.add(filler, new CC().grow().wrap());

		panel.kasse.setFont(panel.kasse.getFont().deriveFont(Font.ITALIC, 11f));
		panel.kasse.setHorizontalAlignment(JLabel.CENTER);
		panel.add(panel.kasse, new CC().grow().wrap());

		JTable table = new JTable(txTableModel);
		table.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
		JScrollPane scrollPane = new JScrollPane(table);
		table.setFillsViewportHeight(true);
		table.getColumnModel().getColumn(0).setPreferredWidth(1);
		table.getColumnModel().getColumn(1).setPreferredWidth(200);

		configureSorting(txTableModel, table, 0, SortOrder.DESCENDING);

		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer() {
			@Override
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
					boolean hasFocus, int row, int column) {
				int realRow = table.convertRowIndexToModel(row);
				Transaction tx = (Transaction) ((TransactionTableModel) table.getModel()).getTxAt(realRow, column);
				setText(String.valueOf(value).replace("$1$",
						String.valueOf(table.getColumnModel().getColumn(1).getWidth() - 72)));
				setBackground(tx.getColors()[row % 2]);
				table.setRowHeight(row, getMinimumSize().height);
				return this;
			}
		};
		rightRenderer.setVerticalAlignment(JLabel.TOP);
		table.getColumnModel().getColumn(0).setCellRenderer(rightRenderer);
		table.getColumnModel().getColumn(0).setMinWidth(30);
		table.getColumnModel().getColumn(1).setCellRenderer(rightRenderer);
		table.setRowSelectionAllowed(false);
		table.setCellSelectionEnabled(false);
		table.setFocusable(false);

		Icon emptyIcon = new Icon() {
			@Override
			public int getIconHeight() {
				return 0;
			}

			@Override
			public int getIconWidth() {
				return 0;
			}

			@Override
			public void paintIcon(Component c, Graphics g, int x, int y) {
			}
		};
		UIManager.put("Table.ascendingSortIcon", emptyIcon);
		UIManager.put("Table.descendingSortIcon", emptyIcon);

		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent evt) {
				int rawRow = table.rowAtPoint(evt.getPoint());
				if (rawRow != -1) {
					int row = table.convertRowIndexToModel(rawRow);
					if (rawRow >= 0) {
						Transaction tx = (((TransactionTableModel) table.getModel()).getTransaction(row));
						if (tx instanceof Payout) {
							Payout p = (Payout) tx;
							JOptionPane.showMessageDialog(frame, String.format(
									"<html><div width='400 px'>Das Geschenk für <b>%s</b> setzt sich aus folgenden Personen zusammen:<br><br>%s",
									p.getPerson(),
									p.getPayers().stream().map(i -> i.toString()).collect(Collectors.joining("; "))));
						}
					}
				}
			}
		});

		panel.add(scrollPane, new CC().grow().push().wrap());

		return panel;
	}

	private static void update(JLabel bdLabel, Date date) {
		bdLabel.setText(String.format("%1$td/%1$tm/19XX", date));
	}

	private void updateKasse() {
		String kasse = "";
		if (dp.isAuthed()) {
			kasse = String.format("<html>Kasse: <b>%d €</b> (<b>%s €</b> für den nächsten Geburtstag)", dp.getTotal(),
					dp.getMaxAvailable());
		}
		panel1.updateKasse(kasse);
		panel2.updateKasse(kasse);
		panel3.updateKasse(kasse);
		panel4.updateKasse(kasse);

	}

	public void updateTotals() {
		updateKasse();
	}
}
