package de.rpk_aachen.birthday.gui;

import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.text.NumberFormat;
import java.text.ParseException;

import javax.inject.Inject;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.text.NumberFormatter;

import de.rpk_aachen.birthday.core.DataProvider;
import de.rpk_aachen.birthday.model.Person;
import lombok.RequiredArgsConstructor;
import net.miginfocom.layout.CC;
import net.miginfocom.swing.MigLayout;

@RequiredArgsConstructor(onConstructor = @_(@Inject))
public class AddCash {
    private final DataProvider dp;
    private JDialog            frame;
    private JComboBox<Person>  people;
    private BdCalendar         bdCalendar;

    public void createAndShowGui(BdCalendar bdCalendar, Person person) {
        this.bdCalendar = bdCalendar;

        frame = new JDialog((JFrame) null, "Geld Einzahlen");
        frame.setModal(true);
        frame.getRootPane().registerKeyboardAction((ActionEvent ae) -> frame.dispose(),
                KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);

        JPanel panel = (JPanel) frame.getContentPane();
        panel.setLayout(new MigLayout());

        people = new JComboBox<Person>(
                dp.getPeople().stream().sorted((Person p1, Person p2) -> p1.getName().compareTo(p2.getName()))
                        .toArray(size -> new Person[size]));
        panel.add(people, new CC().span(2).grow().wrap());
        if (person != null) {
            people.setSelectedItem(person);
        }

        panel.add(new JLabel("Betrag: "));

        NumberFormat longFormat = NumberFormat.getIntegerInstance();

        NumberFormatter numberFormatter = new NumberFormatter(longFormat);
        numberFormatter.setValueClass(Long.class);
        numberFormatter.setAllowsInvalid(false);
        numberFormatter.setMinimum(0l);

        JFormattedTextField cash = new JFormattedTextField(numberFormatter);
        cash.setValue(0);
        cash.setHorizontalAlignment(SwingConstants.RIGHT);
        cash.addFocusListener(new java.awt.event.FocusAdapter() {
            @Override
            public void focusGained(FocusEvent evt) {
                SwingUtilities.invokeLater(() -> cash.selectAll());
            }
        });
        panel.add(cash, new CC().grow().wrap());

        JButton ok = new JButton("Einzahlen");
        ok.addActionListener((ActionEvent ae) -> depositCash(cash));
        panel.add(ok);

        JButton cancel = new JButton("Abbrechen");
        cancel.addActionListener((ActionEvent ae) -> frame.dispose());
        panel.add(cancel);

        frame.pack();
        frame.setLocationRelativeTo(null);
        cash.requestFocusInWindow();
        frame.setVisible(true);
    }

    private void depositCash(JFormattedTextField cash) {
        try {
            cash.commitEdit();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        int amount = ((Number) cash.getValue()).intValue();
        dp.deposit((Person) people.getSelectedItem(), amount);
        bdCalendar.updateTotals();

        JOptionPane.showMessageDialog(frame,
                String.format("Dem Teilnehmer %s wurden %d € gutgeschrieben.", people.getSelectedItem(), amount));

        frame.dispose();
    }
}
