package de.rpk_aachen.birthday.gui;

import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import javax.inject.Inject;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;

import de.rpk_aachen.birthday.core.DataProvider;
import de.rpk_aachen.birthday.model.tx.Withdrawal;
import lombok.RequiredArgsConstructor;
import net.miginfocom.layout.CC;
import net.miginfocom.swing.MigLayout;

@RequiredArgsConstructor(onConstructor = @_(@Inject))
public class MiscExpense {
	private final DataProvider dp;
	private JDialog frame;
	private BdCalendar bdCalendar;

	public void createAndShowGui(BdCalendar bdCalendar) {
		this.bdCalendar = bdCalendar;

		frame = new JDialog((JFrame) null, "Sonstige Ausgaben");
		frame.setModal(true);
		frame.setResizable(false);
		frame.getRootPane().registerKeyboardAction((ActionEvent ae) -> frame.dispose(),
				KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);

		JPanel panel = (JPanel) frame.getContentPane();
		panel.setLayout(new MigLayout("", "[][grow, fill][100][]", "[][grow, fill][][][]"));

		panel.add(
				new JLabel(
						"<html><p>Sonstige Ausgaben, wie zum Beispiel neue Geburtstagskarten, oder ein Wochenendtrip nach Teneriffa.</p>"),
				new CC().span(3).maxWidth("300").alignY("top"));

		panel.add(new JLabel(new ImageIcon(getClass().getResource("/teneriffe.png"))), new CC().spanY(5).wrap());

		panel.add(new JLabel(), new CC().span(3).minHeight("1").growY().wrap());

		panel.add(new JLabel("Betrag: "), new CC().span(2).alignX("left"));

		NumberFormat longFormat = NumberFormat.getNumberInstance(Locale.GERMANY);

		NumberFormatter numberFormatter = new NumberFormatter(longFormat);
		numberFormatter.setAllowsInvalid(false);
		numberFormatter.setMinimum(0l);

		JFormattedTextField cash = new JFormattedTextField();
		cash.setFormatterFactory(
				new DefaultFormatterFactory(new NumberFormatter(NumberFormat.getInstance(Locale.GERMAN))));
		cash.setValue(0);
		cash.setHorizontalAlignment(SwingConstants.RIGHT);
		panel.add(cash, new CC().alignX("right").grow().wrap());

		panel.add(new JLabel("Zweck: "), new CC().alignX("left"));

		JTextField use = new JTextField();
		use.setText("Geburtstagskarten");
		use.setHorizontalAlignment(SwingConstants.RIGHT);
		use.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent evt) {
				SwingUtilities.invokeLater(() -> use.selectAll());
			}
		});
		panel.add(use, new CC().alignX("right").span(2).grow().wrap());

		JButton ok = new JButton("Auszahlen");
		ok.addActionListener((ActionEvent ae) -> withdrawCash(cash, use.getText()));
		panel.add(ok, new CC().span(2));

		JButton cancel = new JButton("Abbrechen");
		cancel.addActionListener((ActionEvent ae) -> frame.dispose());
		panel.add(cancel, new CC().alignX("right").grow());

		frame.pack();
		frame.setLocationRelativeTo(null);
		cash.requestFocusInWindow();
		frame.setVisible(true);
	}

	private void withdrawCash(JFormattedTextField cash, String use) {
		try {
			cash.commitEdit();
		} catch (ParseException e) {
			e.printStackTrace();
		}

		BigDecimal bdAmount = new BigDecimal(cash.getText().replace(".", "").replace(",", "."));
		int amount = bdAmount.multiply(BigDecimal.valueOf(100)).intValue();
		dp.withdraw(amount, new Withdrawal(amount, use));
		bdCalendar.updateTotals();

		JOptionPane.showMessageDialog(frame,
				String.format("Es wurden %.2f € ausgezahlt; Verwendungszweck: %s.", bdAmount.doubleValue(), use));

		frame.dispose();
	}
}
