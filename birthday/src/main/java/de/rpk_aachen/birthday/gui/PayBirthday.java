package de.rpk_aachen.birthday.gui;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.MonthDay;

import javax.inject.Inject;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.text.NumberFormatter;

import de.rpk_aachen.birthday.core.DataProvider;
import de.rpk_aachen.birthday.model.BirthdayTableModel;
import de.rpk_aachen.birthday.model.Person;
import lombok.RequiredArgsConstructor;
import net.miginfocom.layout.CC;
import net.miginfocom.swing.MigLayout;

@RequiredArgsConstructor(onConstructor = @_(@Inject))
public class PayBirthday {
    private final DataProvider dp;
    private JDialog            frame;
    private JComboBox<Person>  people;
    private BdCalendar         bdCalendar;

    @SuppressWarnings("serial")
    public void createAndShowGui(BdCalendar bdCalendar, Person person) {
        this.bdCalendar = bdCalendar;

        frame = new JDialog((JFrame) null, "Geburtstag auszahlen");
        frame.setModal(true);
        frame.getRootPane().registerKeyboardAction((ActionEvent ae) -> frame.dispose(),
                KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);

        JPanel panel = (JPanel) frame.getContentPane();
        panel.setLayout(new MigLayout());

        panel.add(
                new JLabel(String.format("Für den nächsten Geburtstag stehen maximal %d € zur Verfügung.",
                        dp.getMaxAvailable()), SwingConstants.CENTER),
                new CC().span(4).spanX().alignX("center").wrap());

        Person[] peopleList = dp.getPeople().stream().sorted(
                (Person p1, Person p2) -> MonthDay.from(p1.getBirthday()).compareTo(MonthDay.from(p2.getBirthday())))
                .toArray(size -> new Person[size]);
        people = new JComboBox<Person>(peopleList);
        people.setRenderer(new DefaultListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(@SuppressWarnings("rawtypes") JList list, Object value,
                    int index, boolean isSelected, boolean cellHasFocus) {
                if (value instanceof Person) {
                    LocalDate d = ((Person) value).getBirthday();
                    value = d.format(BirthdayTableModel.formatter) + ", " + ((Person) value).getName();
                }
                super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                return this;
            }
        });
        panel.add(people, new CC().span(4).grow().wrap());
        for (Person p : peopleList) {
            if (MonthDay.from(p.getBirthday()).compareTo(MonthDay.now()) <= 0) {
                people.setSelectedItem(p);
            }
        }
        if (person != null) {
            people.setSelectedItem(person);
        }

        panel.add(new JLabel("Betrag: "));

        NumberFormat longFormat = NumberFormat.getIntegerInstance();

        NumberFormatter numberFormatter = new NumberFormatter(longFormat);
        numberFormatter.setValueClass(Long.class);
        numberFormatter.setAllowsInvalid(false);
        numberFormatter.setMinimum(0l);

        JFormattedTextField cash = new JFormattedTextField(numberFormatter);
        cash.setValue(dp.getMaxAvailable());
        cash.setHorizontalAlignment(SwingConstants.RIGHT);
        panel.add(cash, new CC().span(3).grow().wrap());

        JButton ok = new JButton("Auszahlen");
        ok.addActionListener((ActionEvent ae) -> payout(cash));
        panel.add(ok, new CC().span(2).grow());

        JButton cancel = new JButton("Abbrechen");
        cancel.addActionListener((ActionEvent ae) -> frame.dispose());
        panel.add(cancel, new CC().span(2).grow());

        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    private void payout(JFormattedTextField cash) {
        try {
            cash.commitEdit();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        int amount = ((Number) cash.getValue()).intValue();

        try {
            amount = dp.payout((Person) people.getSelectedItem(), amount);
        } catch (IllegalArgumentException iae) {
            JOptionPane.showMessageDialog(frame,
                    String.format(
                            "<html><p style='width: 400px;' align='center'>Es ist nicht möglich, <b>%d €</b> "
                                    + "an %s auszuzahlen. Von jedem Teilnehmer kann pro Geburtstag maximal <b>1 €</b> "
                                    + "ausgezahlt werden. Momentan sind das nur <b>%d €</b>.",
                            amount, people.getSelectedItem(), dp.getMaxAvailable()),
                    "Fehler", JOptionPane.ERROR_MESSAGE);
            return;
        }

        JOptionPane.showMessageDialog(frame,
                String.format("<html>Dem Teilnehmer %s wurden <b>%d €</b> zum Geburtstag geschenkt.",
                        people.getSelectedItem(), amount));
        bdCalendar.updateTotals();

        frame.dispose();
    }
}
