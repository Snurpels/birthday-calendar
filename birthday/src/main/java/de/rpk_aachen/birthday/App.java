package de.rpk_aachen.birthday;

import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import javax.inject.Inject;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.WindowConstants;

import org.codejargon.feather.Feather;
import org.codejargon.feather.Provides;

import de.rpk_aachen.birthday.core.DataProvider;
import de.rpk_aachen.birthday.core.RestDataProvider;
import de.rpk_aachen.birthday.gui.AddCash;
import de.rpk_aachen.birthday.gui.BdCalendar;
import de.rpk_aachen.birthday.gui.PasswordPanel;
import net.miginfocom.layout.AC;
import net.miginfocom.layout.CC;
import net.miginfocom.layout.LC;
import net.miginfocom.swing.MigLayout;

public class App {
	public static void main(String[] args)
			throws InvocationTargetException, InterruptedException, FileNotFoundException, IOException {
		try {
			UIManager.setLookAndFeel("com.seaglasslookandfeel.SeaGlassLookAndFeel");
		} catch (Exception e) {
			e.printStackTrace();
		}

		// // Get password from user
		String password;
		PasswordPanel pPnl = new PasswordPanel();
		JOptionPane op = new JOptionPane(pPnl, JOptionPane.QUESTION_MESSAGE, JOptionPane.PLAIN_MESSAGE,
				new ImageIcon(new ImageIcon(App.class.getResource("/bdc.png")).getImage().getScaledInstance(128, 128,
						Image.SCALE_SMOOTH)));

		JDialog dlg = op.createDialog("Passwort zum ändern?");
		Image icon = new ImageIcon(App.class.getResource("/bdc.png")).getImage();
		dlg.setIconImage(icon);

		// Wire up FocusListener to ensure JPasswordField is able to request
		// focus when the dialog is first shown.
		dlg.addWindowFocusListener(new WindowAdapter() {
			@Override
			public void windowGainedFocus(WindowEvent e) {
				pPnl.gainedFocus();
			}
		});

		SwingUtilities.invokeAndWait(() -> dlg.setVisible(true));

		if (op.getValue() != null && op.getValue().equals(JOptionPane.OK_OPTION)) {
			password = new String(pPnl.getPassword());
		} else {
			password = "";
		}

		JDialog progress = new JDialog((JFrame) null, "Datentransfer", false);
		JProgressBar dpb = new JProgressBar();
		dpb.setIndeterminate(true);
		progress.setLayout(new MigLayout(new LC().gridGap("15", "15").insets("15"), new AC().grow().fill()));
		progress.add(new JLabel("Datenübertragung zum Server..."), new CC().pushX().growX().alignX("center").wrap());
		progress.add(dpb);
		progress.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		progress.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		progress.pack();
		// progress.setIconImage(icon);
		progress.setLocationRelativeTo(null);

		SwingUtilities.invokeAndWait(() -> progress.setVisible(true));

		Feather feather = Feather.with(new Object() {
			@Provides
			@Inject
			AddCash ac(DataProvider dp) {
				return new AddCash(dp);
			}

			@Provides
			DataProvider dp() {
				return new RestDataProvider(password);
			}
		});

		BdCalendar cal = feather.instance(BdCalendar.class);

		SwingUtilities.invokeLater(() -> {
			cal.createAndShowGui();
			progress.setVisible(false);
		});
	}
}
