package de.rpk_aachen.birthday.model.tx;

import java.awt.Color;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import de.rpk_aachen.birthday.model.Person;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class Payout extends Transaction {
    @Getter
    @Setter
    private int           amount;
    @Getter
    private List<Person>  payers;

    @JsonIgnore
    private final Color[] colors = { new Color(255, 210, 210), new Color(230, 180, 180) };

    public Payout(Person person, int amount, List<Person> payers) {
        this.person = person;
        this.amount = amount;
        this.payers = payers;
    }

    @Override
    public String toString() {
        return String.format(
                "<html><body style='width:$1$px'><i><b>%s</b> wurden zum Geburtstag <b>%d €</b> geschenkt. Für eine Liste bitte anklicken.",
                person.getName(), amount);
    }

    @Override
    public Color[] getColors() {
        return colors;
    }
}
