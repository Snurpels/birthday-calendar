package de.rpk_aachen.birthday.model.tx;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

@SuppressWarnings("serial")
public class TransactionTableModel extends AbstractTableModel {
    private List<Transaction> transactions = new ArrayList<>();

    @Override
    public String getColumnName(int col) {
        return col == 0 ? "Datum" : "Transaktionen";
    }

    @Override
    public int getRowCount() {
        return transactions.size();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (columnIndex == 0) return transactions.get(rowIndex).getDate().format(Transaction.formatter);
        Transaction tx = transactions.get(rowIndex);
        return tx.toString();
    }

    public Object getTxAt(int rowIndex, int columnIndex) {
        return transactions.get(rowIndex);
    }

    public void addTransaction(Transaction tx) {
        transactions.add(tx);
        fireTableDataChanged();
    }

    public Transaction getTransaction(int row) {
        return transactions.get(row);
    }

    public void addAll(List<Transaction> transactions) {
        this.transactions.addAll(transactions);
    }
}
