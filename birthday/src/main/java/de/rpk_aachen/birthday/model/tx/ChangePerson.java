package de.rpk_aachen.birthday.model.tx;

import java.awt.Color;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnore;

import de.rpk_aachen.birthday.model.Person;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class ChangePerson extends Transaction {
    @JsonIgnore
    private final static Color[] colors = { new Color(110, 110, 155), new Color(80, 80, 130) };
    @Getter
    private Person               personNew;

    public ChangePerson(Person personOld, Person personNew) {
        this.person = personOld;
        this.personNew = personNew;
    }

    public ChangePerson(Person personOld, Person personNew, LocalDateTime date) {
        this(personOld, personNew);
        this.setDate(date);
    }

    @Override
    @JsonIgnore
    public Color[] getColors() {
        return colors;
    }

    @Override
    public String toString() {
        return String.format(
                "<html><body style='width:$1$px'><b>%s</b> (<b>%,d €</b>, %3$td/%3$tm/19XX%4$s) wurde zu <b>%5$s</b> "
                        + "(<b>%6$,d €</b>, %7$td/%7$tm/19XX%8$s) geändert.",
                person.getName(), person.getEuros(), person.getBirthday(), person.isAlumn() ? ", ex" : "",
                personNew.getName(), personNew.getEuros(), personNew.getBirthday(), personNew.isAlumn() ? ", ex" : "");
    }
}
