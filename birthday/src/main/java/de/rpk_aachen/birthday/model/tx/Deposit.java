package de.rpk_aachen.birthday.model.tx;

import java.awt.Color;

import com.fasterxml.jackson.annotation.JsonIgnore;

import de.rpk_aachen.birthday.model.Person;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class Deposit extends Transaction {
    @Getter
    @Setter
    private int           amount;

    @JsonIgnore
    private final Color[] colors = { new Color(210, 255, 210), new Color(180, 230, 180) };

    public Deposit(Person person, int amount) {
        this.person = person;
        this.amount = amount;
    }

    @Override
    public String toString() {
        return String.format("<html><body style='width:$1$px'><b>%s</b> hat <b>%d €</b> eingezahlt.</body></html>",
                person.getName(), amount, person.getBirthday());
    }

    @Override
    public Color[] getColors() {
        return colors;
    }
}
