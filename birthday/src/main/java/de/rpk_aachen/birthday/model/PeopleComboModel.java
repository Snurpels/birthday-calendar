package de.rpk_aachen.birthday.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

import de.rpk_aachen.birthday.gui.BdCalendar.PeopleWrapper;
import de.rpk_aachen.birthday.model.tx.ChangePerson;

@SuppressWarnings("serial")
public class PeopleComboModel extends AbstractListModel<PeopleWrapper> implements ComboBoxModel<PeopleWrapper> {
    private final List<PeopleWrapper> people   = new ArrayList<>();
    private int                       selected = -1;

    public void addPerson(Person person) {
        people.add(new PeopleWrapper(person));
        this.people.sort((pw1, pw2) -> pw1.getPerson().getName().compareToIgnoreCase(pw2.getPerson().getName()));
    }

    @Override
    public int getSize() {
        return people.size();
    }

    @Override
    public PeopleWrapper getElementAt(int index) {
        return people.get(index);
    }

    @Override
    public void setSelectedItem(Object anItem) {
        if (anItem != null) {
            for (int i = 0; i < people.size(); i++) {
                if (anItem.equals(people.get(i))) {
                    selected = i;
                    break;
                }
            }
        }
    }

    @Override
    public Object getSelectedItem() {
        return selected == -1 ? null : people.get(selected);
    }

    public void addAll(List<Person> people) {
        this.people.clear();
        if (people.size() != 0) {
            this.people.addAll(people.stream().map(p -> new PeopleWrapper(p)).collect(Collectors.toList()));
        }
        this.people.sort((pw1, pw2) -> pw1.getPerson().getName().compareToIgnoreCase(pw2.getPerson().getName()));
    }

    public void changePerson(ChangePerson tx) {
        people.remove(new PeopleWrapper(tx.getPerson()));
        addPerson(tx.getPersonNew());
    }
}
