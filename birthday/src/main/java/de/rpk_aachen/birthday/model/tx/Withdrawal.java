package de.rpk_aachen.birthday.model.tx;

import java.awt.Color;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class Withdrawal extends Transaction {
	@Getter
	@Setter
	private int amount;
	@Getter
	@Setter
	private String reason;

	@JsonIgnore
	private final Color[] colors = { new Color(255, 210, 210), new Color(230, 180, 180) };

	public Withdrawal(int amount, String reason) {
		this.amount = amount;
		this.reason = reason;
	}

	@Override
	public String toString() {
		return String.format(
				"<html><body style='width:$1$px'><b>%.2f €</b> wurden abgehoben. Verwendungszweck: <i>%s</s>",
				new BigDecimal(amount).divide(new BigDecimal(100)).doubleValue(), reason);
	}

	@Override
	public Color[] getColors() {
		return colors;
	}
}
