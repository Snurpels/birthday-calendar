package de.rpk_aachen.birthday.model;

import lombok.Data;

@Data
public class ModifiedPerson {
    private final Person oldPerson, newPerson;
}
