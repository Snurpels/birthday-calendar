package de.rpk_aachen.birthday.model.tx;

import java.awt.Color;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import de.rpk_aachen.birthday.model.Person;
import lombok.Getter;
import lombok.Setter;

public abstract class Transaction {
    protected static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd kk:mm:ss");

    @Getter
    @Setter
    private LocalDateTime                    date      = LocalDateTime.now();

    @Getter
    protected Person                         person;

    @Override
    public abstract String toString();

    public abstract Color[] getColors();
}
