package de.rpk_aachen.birthday.model;

import java.awt.Color;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.swing.table.AbstractTableModel;

import de.rpk_aachen.birthday.core.Mode;
import lombok.RequiredArgsConstructor;

@SuppressWarnings("serial")
@RequiredArgsConstructor
public class BirthdayTableModel extends AbstractTableModel {
    public static final DateTimeFormatter formatter         = DateTimeFormatter.ofPattern("'19XX'-MM-dd");
    private final String[]                columnNames       = { "Name", "Guthaben", "Geburtstag" };
    private final NumberFormat            currencyFormatter = NumberFormat.getCurrencyInstance(Locale.getDefault());
    private List<Person>                  people            = new ArrayList<>();
    private final boolean                 authed;

    public void addAll(List<Person> people) {
        this.people.addAll(people);
    }

    public void addEuros(Person person, int euros) {
        // Terrible
        for (Person p : people)
            if (p.getName().equals(person.getName())) {
                p.addEuros(euros);
            }

        fireTableDataChanged();
    }

    public void addPerson(Person p) {
        people.add(p);
        fireTableDataChanged();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int col) {
        return columnNames[col].toString();
    }

    public Person getPerson(int row) {
        return people.get(row);
    }

    public Color getRowColor(Mode mode, int row, boolean authed) {
        LocalDate today = LocalDate.now();
        switch (mode) {
        case CHRONO: {
            if (people.get(row).getBirthday().getMonth() == today.getMonth()) return Color.decode("#C4DFE6");
            if (people.get(row).getBirthday().getMonth().compareTo(today.getMonth()) > 0)
                return Color.decode("#66A5AD").brighter();
            return Color.decode("#66A5AD");
        }
        case CREDIT:
            if (!authed)
                return new Color(150, 255, 150);
            else
                return people.get(row).getEuros() >= 2 ? new Color(150, 255, 150) : new Color(255, 150, 150);
        default:
            return Color.BLACK;
        }
    }

    @Override
    public int getRowCount() {
        return people.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Person person = people.get(rowIndex);
        switch (columnIndex) {
        case 0:
            return person.getName();
        case 1:
            if (!authed) return "2€ oder mehr";
            return currencyFormatter.format(person.getEuros());
        case 2:
            return person.getBirthday().format(formatter);
        }

        return null;
    }

    public void removePerson(Person p) {
        people.remove(p);
        fireTableDataChanged();
    }

    public boolean thisMonth(int row) {
        return people.get(row).getBirthday().getMonth() == LocalDate.now().getMonth();
    }
}
