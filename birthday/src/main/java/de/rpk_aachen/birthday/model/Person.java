package de.rpk_aachen.birthday.model;

import java.time.LocalDate;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Person {
    private String    name;
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    private LocalDate birthday;
    private boolean   alumn;
    private int       euros;

    public void addEuros(int i) {
        euros += i;
    }

    @Override
    public String toString() {
        return name;
    }
}
