package de.rpk_aachen.birthday.model.tx;

import java.awt.Color;

import com.fasterxml.jackson.annotation.JsonIgnore;

import de.rpk_aachen.birthday.model.Person;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class AddPerson extends Transaction {
    @JsonIgnore
    private final static Color[] colors = { new Color(210, 210, 255), new Color(180, 180, 230) };

    public AddPerson(Person person) {
        this.person = person;
    }

    @Override
    public String toString() {
        return String.format(
                "<html><body style='width:$1$px'><b>%s</b> wurde mit <b>%d €</b> und Geburtstag am %3$td/%3$tm/19XX hinzugefügt.",
                person.getName(), person.getEuros(), person.getBirthday());
    }

    @Override
    @JsonIgnore
    public Color[] getColors() {
        return colors;
    }
}
