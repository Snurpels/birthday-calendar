package de.rpk_aachen.birthday.model.rest;

public class Endpoints {
	public static final String GET_AUTHORIZED = "getAuthorized";
	public static final String GET_CHANGEPEOPLE = "getChangePeople";

	public static final String PEOPLE = "people";
	public static final String TX_ADDPERSON = "tx/addPeople";
	public static final String TX_DEPOSIT = "tx/deposit";
	public static final String TX_PAYOUT = "tx/payout";
	public static final String TX_WITHDRAWAL = "tx/withdrawal";
}
