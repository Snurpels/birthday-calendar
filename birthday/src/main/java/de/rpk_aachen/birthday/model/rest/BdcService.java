package de.rpk_aachen.birthday.model.rest;

import static de.rpk_aachen.birthday.model.rest.Endpoints.GET_AUTHORIZED;
import static de.rpk_aachen.birthday.model.rest.Endpoints.GET_CHANGEPEOPLE;
import static de.rpk_aachen.birthday.model.rest.Endpoints.PEOPLE;
import static de.rpk_aachen.birthday.model.rest.Endpoints.TX_ADDPERSON;
import static de.rpk_aachen.birthday.model.rest.Endpoints.TX_DEPOSIT;
import static de.rpk_aachen.birthday.model.rest.Endpoints.TX_PAYOUT;
import static de.rpk_aachen.birthday.model.rest.Endpoints.TX_WITHDRAWAL;

import java.util.List;

import de.rpk_aachen.birthday.model.ModifiedPerson;
import de.rpk_aachen.birthday.model.Person;
import de.rpk_aachen.birthday.model.tx.AddPerson;
import de.rpk_aachen.birthday.model.tx.ChangePerson;
import de.rpk_aachen.birthday.model.tx.Deposit;
import de.rpk_aachen.birthday.model.tx.Payout;
import de.rpk_aachen.birthday.model.tx.Withdrawal;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface BdcService {
	@POST(PEOPLE)
	Call<Boolean> addPerson(@Query("p") String password, @Body Person person);

	@POST(TX_ADDPERSON)
	Call<Boolean> addTxAddPerson(@Query("p") String password, @Body AddPerson addPerson);

	@POST(TX_DEPOSIT)
	Call<Boolean> addTxDeposit(@Query("p") String password, @Body Deposit deposit);

	@POST(TX_PAYOUT)
	Call<Boolean> addTxPayout(@Query("p") String password, @Body Payout payout);

	@GET(TX_ADDPERSON)
	Call<List<AddPerson>> getAddPeople(@Query("p") String password);

	@GET(GET_AUTHORIZED)
	Call<Boolean> getAuthorized(@Query("p") String password);

	@GET(GET_CHANGEPEOPLE)
	Call<List<ChangePerson>> getChangePeople(@Query("p") String password);

	@GET(TX_DEPOSIT)
	Call<List<Deposit>> getDeposit(@Query("p") String password);

	@GET(TX_WITHDRAWAL)
	Call<List<Withdrawal>> getWithdrawal(@Query("p") String password);

	@GET(TX_PAYOUT)
	Call<List<Payout>> getPayout(@Query("p") String password);

	@GET(PEOPLE)
	Call<List<Person>> getPeople(@Query("p") String password);

	@PUT(PEOPLE)
	Call<Boolean> updatePerson(@Query("p") String password, @Body ModifiedPerson mf);

	@POST(TX_WITHDRAWAL)
	Call<Boolean> addTxWithdrawal(@Query("p") String password, @Body Withdrawal withdrawal);
}
