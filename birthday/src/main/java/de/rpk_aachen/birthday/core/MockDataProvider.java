package de.rpk_aachen.birthday.core;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import de.rpk_aachen.birthday.model.BirthdayTableModel;
import de.rpk_aachen.birthday.model.PeopleComboModel;
import de.rpk_aachen.birthday.model.Person;
import de.rpk_aachen.birthday.model.tx.AddPerson;
import de.rpk_aachen.birthday.model.tx.Deposit;
import de.rpk_aachen.birthday.model.tx.Payout;
import de.rpk_aachen.birthday.model.tx.Transaction;
import de.rpk_aachen.birthday.model.tx.TransactionTableModel;
import de.rpk_aachen.birthday.model.tx.Withdrawal;
import lombok.Getter;

public class MockDataProvider implements DataProvider {
    private BirthdayTableModel    tableModel;
    private TransactionTableModel txTableModel;

    @Getter
    List<Person>                  people       = new ArrayList<>();
    @Getter
    List<Transaction>             transactions = new ArrayList<>();

    public MockDataProvider() {
        String[] firstNames = { "Lenz", "Hubert", "Gottlob", "Martin", "Michel", "David", "Heiner", "Ernst", "Florian",
                "Nikolaus", "Fred", "Arend", };
        String[] lastNames = { "Jollenbeck", "Fried", "Loris", "Buchholz", "Grosse", "Strohkirch", "Bambach", "Ott",
                "Schubert", "Wedekind", "Houk", "Löwe", };

        Random rng = new Random(1);

        Set<String> names = new HashSet<String>();
        String name;
        for (int i = 0; i < 40; i++) {
            do {
                name = String.format("%s, %s", lastNames[rng.nextInt(lastNames.length)],
                        firstNames[rng.nextInt(firstNames.length)]);
            } while (names.contains(name));
            names.add(name);
            Person p = new Person(name, LocalDate.of(2012, (rng.nextInt(12) + 1), (rng.nextInt(28) + 1)),
                    rng.nextInt(10) < 8, (rng.nextInt(12) + 1));

            people.add(p);
            transactions.add(new AddPerson(p));
        }
    }

    @Override
    public void deposit(Person person, int amount) {
        tableModel.addEuros(person, amount);
        txTableModel.addTransaction(new Deposit(person, amount));
        txTableModel.fireTableDataChanged();
    }

    @Override
    public void registerModel(BirthdayTableModel tableModel) {
        this.tableModel = tableModel;
    }

    @Override
    public int getMaxAvailable() {
        return getMaxAvailable(null);
    }

    @Override
    public int getMaxAvailable(Person p1) {
        int available = 0;
        for (Person p2 : people) {
            if (!p2.equals(p1) && p2.getEuros() > 0) {
                available++;
            }
        }
        return available;
    }

    @Override
    public int payout(Person person, int amount) {
        if (amount > getMaxAvailable())
            throw new IllegalArgumentException("Unable to pay out more than " + getMaxAvailable());

        int payout = 0;
        List<Person> payers = new ArrayList<>();
        for (Person p : people) {
            if (p.getEuros() > 0) {
                p.setEuros(p.getEuros() - 1);
                payout++;
                payers.add(p);
            }
        }

        Payout tx = new Payout(person, payout, payers);
        transactions.add(tx);
        txTableModel.addTransaction(tx);
        txTableModel.fireTableDataChanged();

        return payout;
    }

    @Override
    public int getTotal() {
        int total = 0;
        for (Person p : people) {
            if (p.getEuros() > 0) {
                total += p.getEuros();
            }
        }
        return total;
    }

    @Override
    public boolean addPerson(Person person) {
        people.add(person);
        tableModel.addPerson(person);
        tableModel.fireTableDataChanged();
        transactions.add(new AddPerson(person));

        return true;
    }

    @Override
    public void registerModel(TransactionTableModel txTableModel) {
        this.txTableModel = txTableModel;
    }

    @Override
    public void setPassword(String password) {}

    @Override
    public boolean isAuthed() {
        return true;
    }

    @Override
    public boolean changePerson(Person pOld, Person pNew) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void registerModel(PeopleComboModel cmbModel) {
        // TODO Auto-generated method stub

    }

	@Override
	public void withdraw(int amount, Withdrawal tx) {
		// TODO Auto-generated method stub
		
	}
}
