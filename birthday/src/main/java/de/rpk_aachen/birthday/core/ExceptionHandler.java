package de.rpk_aachen.birthday.core;

import java.awt.Frame;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.stream.Stream;

import javax.swing.JOptionPane;

public class ExceptionHandler implements Thread.UncaughtExceptionHandler {
    private static Frame getFrame() {
        return Stream.of(Frame.getFrames()).filter(f -> f.isVisible()).findFirst().get();
    }

    public static void registerExceptionHandler() {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler());
        System.setProperty("sun.awt.exception.handler", ExceptionHandler.class.getName());
    }

    private SimpleDateFormat filenameFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");

    private String getStackTrace(Throwable throwable) {
        Writer result = new StringWriter();
        throwable.printStackTrace(new PrintWriter(result));
        return result.toString();
    }

    public void handle(Throwable e) {
        try {
            File logfile = new File(System.getProperty("user.home"), filenameFormat.format(new Date()) + ".log");
            FileOutputStream fos = new FileOutputStream(logfile);
            PrintWriter pw = new PrintWriter(fos);
            e.printStackTrace(pw);
            pw.flush();
            e.printStackTrace();
        } catch (Exception ignored) { /* Can't do anything at this point */ }
        StringBuilder sb = new StringBuilder();
        sb.append("<html><body>");
        sb.append(String.format("<b>Stacktrace:</b>%s", getStackTrace(e)));
        JOptionPane.showMessageDialog(getFrame(), sb.toString(), "Error", JOptionPane.ERROR_MESSAGE);
    }

    @Override
    public void uncaughtException(Thread t, Throwable e) {
        handle(e);
    }
}