package de.rpk_aachen.birthday.core;

import java.util.List;

import javax.inject.Singleton;

import de.rpk_aachen.birthday.model.BirthdayTableModel;
import de.rpk_aachen.birthday.model.PeopleComboModel;
import de.rpk_aachen.birthday.model.Person;
import de.rpk_aachen.birthday.model.tx.Transaction;
import de.rpk_aachen.birthday.model.tx.TransactionTableModel;
import de.rpk_aachen.birthday.model.tx.Withdrawal;

@Singleton
public interface DataProvider {
    List<Person> getPeople();

    void deposit(Person person, int amount);

    void registerModel(BirthdayTableModel tableModel);

    int getMaxAvailable();

    int getMaxAvailable(Person p);

    int payout(Person person, int amount);

    int getTotal();

    boolean addPerson(Person person);

    void registerModel(TransactionTableModel txTableModel);

    List<Transaction> getTransactions();

    void setPassword(String password);

    boolean isAuthed();

    boolean changePerson(Person pOld, Person pNew);

    void registerModel(PeopleComboModel cmbModel);

	void withdraw(int amount, Withdrawal tx);
}
