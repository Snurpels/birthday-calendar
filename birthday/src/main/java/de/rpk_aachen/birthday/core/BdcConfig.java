package de.rpk_aachen.birthday.core;

import org.aeonbits.owner.Accessible;
import org.aeonbits.owner.Config.Sources;

@Sources({ "file:~/.bdc.config" })
public interface BdcConfig extends Accessible {
    @Key("server.hostname")
    @DefaultValue("192.168.189.201")
    public String hostname();

    @Key("server.port")
    @DefaultValue("8080")
    public int port();
}