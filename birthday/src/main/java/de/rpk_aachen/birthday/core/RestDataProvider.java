package de.rpk_aachen.birthday.core;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;

import org.aeonbits.owner.ConfigFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import de.rpk_aachen.birthday.model.BirthdayTableModel;
import de.rpk_aachen.birthday.model.ModifiedPerson;
import de.rpk_aachen.birthday.model.PeopleComboModel;
import de.rpk_aachen.birthday.model.Person;
import de.rpk_aachen.birthday.model.rest.BdcService;
import de.rpk_aachen.birthday.model.tx.AddPerson;
import de.rpk_aachen.birthday.model.tx.ChangePerson;
import de.rpk_aachen.birthday.model.tx.Deposit;
import de.rpk_aachen.birthday.model.tx.Payout;
import de.rpk_aachen.birthday.model.tx.Transaction;
import de.rpk_aachen.birthday.model.tx.TransactionTableModel;
import de.rpk_aachen.birthday.model.tx.Withdrawal;
import lombok.Getter;
import okhttp3.OkHttpClient;
import okhttp3.OkHttpClient.Builder;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class RestDataProvider implements DataProvider {
	@Getter
	List<Person> people;
	private BirthdayTableModel tableModel;

	@Getter
	List<Transaction> transactions = new ArrayList<>();
	private TransactionTableModel txTableModel;
	private final BdcService bdcService;
	private String password;
	private PeopleComboModel cmbModel;

	public RestDataProvider(String password) {
		this.password = password;

		BdcConfig cfg = ConfigFactory.create(BdcConfig.class);

		Builder httpClient = new OkHttpClient.Builder();
		httpClient.readTimeout(30, TimeUnit.SECONDS);
		httpClient.connectTimeout(30, TimeUnit.SECONDS);

		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());
		Retrofit retrofit = new Retrofit.Builder().client(httpClient.build())
				.baseUrl("http://" + cfg.hostname() + ":" + cfg.port() + "/server/rest/")
				.addConverterFactory(JacksonConverterFactory.create(mapper)).build();

		bdcService = retrofit.create(BdcService.class);

		try {
			Response<List<Person>> response;
			response = bdcService.getPeople(password).execute();
			people = response.body();
		} catch (IOException e) {
			// If this fails, we encountered a critical error (server
			// unavailable) and need to shut down

			JOptionPane.showMessageDialog(null,
					"Der Geburtstags-Server ist leider grade nicht verfügbar. Bitte bei Herrn Treutler melden.",
					"Serverproblem", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}

		try {
			add(transactions, bdcService.getAddPeople(password));
			add(transactions, bdcService.getChangePeople(password));
			add(transactions, bdcService.getDeposit(password));
			add(transactions, bdcService.getWithdrawal(password));
			add(transactions, bdcService.getPayout(password));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private <T extends Transaction> void add(Collection<Transaction> tx, Call<List<T>> addPeople) throws IOException {
		List<T> in = addPeople.execute().body();
		if (in != null) {
			tx.addAll(in);
		}
	}

	@Override
	public boolean addPerson(Person person) {
		try {
			if (bdcService.addPerson(password, person).execute().body()) {
				people.add(person);
				tableModel.addPerson(person);

				AddPerson tx = new AddPerson(person);
				bdcService.addTxAddPerson(password, tx).execute();
				transactions.add(tx);
				txTableModel.addTransaction(tx);

				cmbModel.addPerson(person);

				return true;
			} else {
				JOptionPane.showMessageDialog(null, "Das Passwort erlaubt es leider nicht, Leute hinzuzufügen.",
						"Serverproblem", JOptionPane.ERROR_MESSAGE);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return false;
	}

	@Override
	public void deposit(Person person, int amount) {
		Deposit tx = new Deposit(person, amount);
		try {
			if (bdcService.addTxDeposit(password, tx).execute().body()) {
				tableModel.addEuros(person, amount);
				txTableModel.addTransaction(tx);
			} else {
				JOptionPane.showMessageDialog(null, "Das Passwort erlaubt es leider nicht, Geld hinzuzufügen.",
						"Serverproblem", JOptionPane.ERROR_MESSAGE);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public int getMaxAvailable() {
		return getMaxAvailable(null);
	}

	@Override
	public int getMaxAvailable(Person p1) {
		int available = 0;
		for (Person p2 : people) {
			if (!p2.equals(p1) && p2.getEuros() > 0) {
				available++;
			}
		}
		return available;
	}

	@Override
	public int getTotal() {
		int total = 0;
		for (Person p : people) {
			if (p.getEuros() > 0) {
				total += p.getEuros();
			}
		}
		return total;
	}

	@Override
	public int payout(Person person, int amount) {
		if (amount > getMaxAvailable())
			throw new IllegalArgumentException("Unable to pay out more than " + getMaxAvailable());

		int payout = 0;
		List<Person> payers = new ArrayList<>();
		Collections.shuffle(people);
		for (Person p : people) {
			if (amount == payout) {
				continue;
			}
			if (!p.equals(person) && p.getEuros() > 0) {
				p.setEuros(p.getEuros() - 1);
				payout++;
				payers.add(p);
			} else if (p.equals(person)) {
				person = p;
			}
		}

		// (amount - 1) because nobody pays themselves on their own birthday
		if (payout != amount && payout != (amount - 1))
			throw new Error(String.format("Payout of %d did not match expected payout of %d.", payout, amount));

		Payout tx = new Payout(person, payout, payers);

		try {
			if (bdcService.addTxPayout(password, tx).execute().body()) {
				transactions.add(tx);
				txTableModel.addTransaction(tx);
			} else {
				JOptionPane.showMessageDialog(null, "Das Passwort erlaubt es leider nicht, Geld auszuzahlen.",
						"Serverproblem", JOptionPane.ERROR_MESSAGE);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return payout;
	}

	@Override
	public void registerModel(BirthdayTableModel tableModel) {
		this.tableModel = tableModel;
	}

	@Override
	public void registerModel(TransactionTableModel txTableModel) {
		this.txTableModel = txTableModel;
	}

	@Override
	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public boolean isAuthed() {
		try {
			return bdcService.getAuthorized(password).execute().body();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean changePerson(Person pOld, Person pNew) {
		try {
			// As updating is essentially nothing BUT a transaction, no separate
			// addTx* is needed
			if (bdcService.updatePerson(password, new ModifiedPerson(pOld, pNew)).execute().body()) {
				people.remove(pOld);
				people.add(pNew);
				tableModel.removePerson(pOld);
				if (!pNew.isAlumn()) {
					tableModel.addPerson(pNew);
				}

				ChangePerson tx = new ChangePerson(pOld, pNew);
				transactions.add(tx);
				txTableModel.addTransaction(tx);

				cmbModel.changePerson(tx);

				return true;
			} else {
				JOptionPane.showMessageDialog(null, "Das Passwort erlaubt es leider nicht, Leute zu ändern.",
						"Serverproblem", JOptionPane.ERROR_MESSAGE);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return false;
	}

	@Override
	public void registerModel(PeopleComboModel cmbModel) {
		this.cmbModel = cmbModel;
		cmbModel.addAll(people);
	}

	@Override
	public void withdraw(int amount, Withdrawal tx) {
		try {
			if (bdcService.addTxWithdrawal(password, tx).execute().body()) {
				txTableModel.addTransaction(tx);
			} else {
				JOptionPane.showMessageDialog(null, "Das Passwort erlaubt es leider nicht, Geld abzuheben.",
						"Serverproblem", JOptionPane.ERROR_MESSAGE);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
