package de.rpk_aachen.birthday.core;

import java.awt.Component;
import java.awt.Font;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import de.rpk_aachen.birthday.model.BirthdayTableModel;
import lombok.RequiredArgsConstructor;

@SuppressWarnings("serial")
@RequiredArgsConstructor
public class ColoredRenderer extends DefaultTableCellRenderer {
    private final Mode    mode;
    private final boolean authed;

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
            int row, int column) {
        BirthdayTableModel model = (BirthdayTableModel) table.getModel();
        int realRow = table.convertRowIndexToModel(row);
        Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        c.setBackground(model.getRowColor(mode, realRow, authed));
        if (model.thisMonth(realRow) && authed) {
            c.setFont(c.getFont().deriveFont(Font.BOLD));
        }
        return c;
    }
}