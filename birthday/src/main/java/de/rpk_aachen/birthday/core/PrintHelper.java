package de.rpk_aachen.birthday.core;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.PrinterJob;
import java.lang.reflect.InvocationTargetException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.TextStyle;
import java.util.List;
import java.util.Locale;

import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Chromaticity;
import javax.print.attribute.standard.MediaPrintableArea;
import javax.print.attribute.standard.OrientationRequested;
import javax.print.attribute.standard.PageRanges;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTable.PrintMode;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

import de.rpk_aachen.birthday.model.Person;

public class PrintHelper {
    private static final int COLUMNS = 17;
    private static final int ROWS    = 32;

    public static class CalendarTableModel implements TableModel {
        private String[][] values;

        public CalendarTableModel(RestDataProvider dp) {
            values = new String[COLUMNS][ROWS];
            for (int i = 0; i < COLUMNS; i++) {
                if (i % 4 == 0) {
                    int j = 0;
                    while (j < ROWS - 1) {
                        values[i][j] = String.valueOf(++j);
                    }
                }
            }
            dp.people.stream().forEach(p -> {
                if (p.isAlumn()) return;
                LocalDate bd = p.getBirthday();
                int column = bd.getMonthValue() + ((bd.getMonthValue() - 1) / 3);
                if (values[column][bd.getDayOfMonth() - 1] == null) {
                    values[column][bd.getDayOfMonth() - 1] = "<html>\u00A0" + p.getName();
                } else {
                    values[column][bd.getDayOfMonth() - 1] = values[column][bd.getDayOfMonth() - 1] + "<br>\u00A0"
                            + p.getName();
                }
            });
        }

        @Override
        public int getRowCount() {
            return ROWS;
        }

        @Override
        public int getColumnCount() {
            return COLUMNS;
        }

        @Override
        public String getColumnName(int columnIndex) {
            return columnIndex % 4 == 0 ? ""
                    : Month.of(columnIndex - (columnIndex / 4)).getDisplayName(TextStyle.FULL, Locale.GERMANY);
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            return String.class;
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return false;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            return values[columnIndex][rowIndex];
        }

        @Override
        public void setValueAt(Object aValue, int rowIndex, int columnIndex) {}

        @Override
        public void addTableModelListener(TableModelListener l) {}

        @Override
        public void removeTableModelListener(TableModelListener l) {}
    }

    private static class CustomTableCellRenderer extends DefaultTableCellRenderer {
        private static final Color SATURDAY = new Color(212, 212, 212);
        private static final Color SUNDAY   = new Color(182, 182, 182);

        private Font               originalFont;
        private Font               customFont;

        public CustomTableCellRenderer(Component component) {
            originalFont = new Font(component.getFont().getName(), component.getFont().getStyle(), 16);
        }

        @Override
        protected void paintComponent(Graphics g) {
            customFont = originalFont;
            if (this.getText() != null && !this.getText().isEmpty()) {
                Dimension bounds = new Dimension(99999, 99999);
                Dimension originalSize = this.getSize();
                this.setFont(originalFont);
                while (bounds.getWidth() > originalSize.getWidth() || bounds.getHeight() > originalSize.getHeight()) {
                    this.setFont(
                            new Font(originalFont.getName(), originalFont.getStyle(), this.getFont().getSize() - 1));
                    this.setSize(this.getPreferredSize());
                    bounds = this.getSize();
                    System.out.println("Resizing! " + this.getText() + " to " + this.getFont().getSize());
                }
                this.setSize(originalSize);
                System.out.println("wee" + this.getText() + ":" + this.getFont().getSize());
            }

            super.paintComponent(g);
        }

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
                int row, int column) {
            Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

            c.setBackground(getWeekdayColor(row, column));
            if (c.getBackground() != Color.CYAN) {
                this.setBorder(BorderFactory.createLineBorder(Color.GRAY));
            } else {
                c.setBackground(Color.WHITE);
            }

            return c;
        }

        private Color getWeekdayColor(int row, int column) {
            LocalDate now = LocalDate.now();
            try {
                switch (now.withMonth(column - (column / 4)).withDayOfMonth(row + 1).getDayOfWeek()) {
                case SATURDAY:
                    return SATURDAY;
                case SUNDAY:
                    return SUNDAY;
                default:
                    return Color.WHITE;
                }
            } catch (DateTimeException ignore) {
                return Color.CYAN;
            }
        }
    }

    public static void main(String[] args) throws InvocationTargetException, InterruptedException {
        JFrame frame = new JFrame("Geburtstagskalender");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        @SuppressWarnings("serial")
        JTable table = new JTable(new CalendarTableModel(new RestDataProvider("kranke banane"))) {
            @Override
            public boolean getScrollableTracksViewportWidth() {
                return getPreferredSize().width < getParent().getWidth();
            }

            @Override
            public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
                Component component = super.prepareRenderer(renderer, row, column);
                int rendererWidth = component.getPreferredSize().width;
                TableColumn tableColumn = getColumnModel().getColumn(column);
                tableColumn.setPreferredWidth(
                        Math.max(rendererWidth + getIntercellSpacing().width + 10, tableColumn.getPreferredWidth()));
                return component;
            }
        };
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(SwingConstants.CENTER);
        table.setDefaultRenderer(table.getColumnClass(1), new CustomTableCellRenderer(table));
        PrinterJob job = PrinterJob.getPrinterJob();
        PageFormat pf = job.defaultPage();
        pf.setOrientation(PageFormat.LANDSCAPE);
        Paper paper = pf.getPaper();
        table.setRowHeight((int) (paper.getHeight() / table.getRowCount()) + 7);
        for (int i = 0; i < COLUMNS; i++) {
            if (i % 4 == 0) {
                table.getColumnModel().getColumn(i).setCellRenderer(centerRenderer);
                table.getColumnModel().getColumn(i).setMaxWidth(22);
            } else {
                table.getColumnModel().getColumn(i).setPreferredWidth((int) ((paper.getWidth()) / 5));
                table.getColumnModel().getColumn(i).setMaxWidth((int) ((paper.getWidth()) / 5));
            }
        }
        table.setIntercellSpacing(new Dimension(-1, -1));
        table.setShowHorizontalLines(false);
        table.setShowVerticalLines(false);

        frame.add(new JScrollPane(table), BorderLayout.CENTER);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                try {
                    PrintRequestAttributeSet set = new HashPrintRequestAttributeSet();
                    set.add(OrientationRequested.LANDSCAPE);
                    set.add(Chromaticity.MONOCHROME);
                    set.add(new MediaPrintableArea(100f, 0f, 2000f, 2000f, MediaPrintableArea.MM));
                    set.add(new PageRanges(1));
                    table.print(PrintMode.FIT_WIDTH, null, null, true, set, true);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        SwingUtilities.invokeAndWait(() -> {
            frame.setVisible(true);
        });
    }

    public static void print(List<Person> people) {
        // TODO Auto-generated method stub
    }
}
