# README #

The client for the [Birthday Calendar Server](https://bitbucket.org/Snurpels/birthday-calendar-server). See the server page for detailed setup instructions. All the client needs configuration-wise is a file named 
`~/.bdc.config` (on Windows, that is `%HOMEPATH%\.bdc.config`, which means the config file resides in the users home directory).

The file is a plain old text file and should only contain these two entries (with the values adjusted to where your server actually listens, obviously):

```
#!text

server.hostname=127.0.0.1
server.port=8080
```